<?php
namespace z0s\Tests;

use League\Container\Container;
use Psr\Container\ContainerInterface;

class FrameworkTest extends \Codeception\Test\Unit
{
    /**
     * @var \z0s\Tests\UnitTester
     */
    protected $tester;

    protected Container $container;

    protected function _before()
    {
        // Bootstrap
        [$bootstrap, $autoloader] = require __DIR__ . '/../app/init.php';

        // Get the container
        $this->container = $bootstrap->getContainer();
    }

    protected function _after()
    {
    }

    public function testIsContainerPsrContainer()
    {
        $this->assertTrue($this->container instanceof ContainerInterface);
    }
}
