<?php

namespace z0s\Cronjobs;

use z0s\Cron\Api\Cronjob;

class TestCronjob extends Cronjob
{
    protected string $cronTime = '* * * * *';

    public function handle(): void
    {
        $this->logger->info('Hi mom');
    }
}
