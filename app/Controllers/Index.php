<?php

namespace z0s\Controllers;

use z0s\RoadRunner\Api\Controller;
use z0s\RoadRunner\Attributes\RouteAttribute;

class Index extends Controller
{
    #[RouteAttribute('/test.json', ['GET'])]
    public function testjson()
    {
        //throw new \Exception('test');
        return $this->json(['message' => 'Hello World!']);
    }

    #[RouteAttribute('/[{name}]', ['GET'])]
    public function index(?string $name = 'Derp')
    {
        return $this->render('index.twig', ['name' => $name]);
    }
}
