<?php

namespace z0s\Jobs;

use z0s\Models\Killmails;
use z0s\RoadRunner\Api\Jobs;

class PopulateEsiKillmail extends Jobs
{
    protected bool $retry = true;
    protected int $retryAttempts = 0;
    protected int $retryDelay = 0;
    protected string $queue = 'default';

    public function __construct(protected Killmails $killmails)
    {
    }

    public function handle(array $payload): void
    {
        $killId = $payload['killId'];
        $hash = $payload['hash'];

        $esiKillmail = \file_get_contents("https://esi.evetech.net/latest/killmails/{$killId}/{$hash}");
        $this->killmails->setData(json_decode($esiKillmail, true));
        $this->killmails->save();

        // Push to parser so it's properly parsed
    }
}
