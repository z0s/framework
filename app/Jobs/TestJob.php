<?php

namespace z0s\Jobs;

use z0s\Models\Users;
use z0s\RoadRunner\Api\Jobs;

class TestJob extends Jobs
{
    protected bool $retry = true;
    protected int $retryAttempts = 0;
    protected int $retryDelay = 0;
    protected string $queue = 'default';

    public function __construct(protected Users $users)
    {
    }

    public function handle(array $payload): void
    {
        $users = $this->users->find(['email' => $payload['email'] ?? '']);
        echo "found {$users->count()} users with email {$payload['email']}";
    }
}
